const { expect } 	= require('chai');
const chai 			= require('chai');
const chaiHttp 		= require('chai-http');

chai.use(chaiHttp)

const PORT 		= process.env.PORT || 8080;
const app 		= `http://localhost:${PORT}/`;

describe('Users', () => {

	// Route - Websites
	it('Status Response /users/websites', async function() {
		chai
			.request(app)
			.get('/users/websites')
			.end(callback => {
			    console.log(callback);
			})
	});

	// Route- Profiles
	it('Status Response /users/profiles', async function() {
		chai
			.request(app)
			.get('/users/profiles')
			.end((err, res) => {
			    expect(res).to.have.status(200);
			    expect(res).to.be.a('json');
			})
	});

	// Route - Address
	it('Status Response /users/address', async function() {
		chai
			.request(app)
			.get('/users/address')
			.end((err, res) => {
			    expect(res).to.have.status(200);
			    expect(res).to.be.a('json');
			})
	});

});

/*

>>> POSTMAN EXAMPLE TEST

pm.test("Response is ok and is json", function () {
    pm.response.to.be.ok;
    pm.response.to.be.json;
});

pm.test("Status 200 - OK", function () {
    pm.response.to.have.status(200);
});

pm.test("Status 400 - Bad Request", function () {
    pm.response.to.have.status(400);
});

*/