# Node Version
FROM node:12

WORKDIR /usr/src/app

# Install app dependencies
COPY package.json ./

RUN npm install

COPY --chown=node:node . .

USER node

# Bundle app source
COPY . .

EXPOSE 8080

CMD [ "node", "app.js" ]