

# Mutant Test - NodeJS

NodeJS application that tests for users and returns treated information.

---

## Documentation

Check the endpoints and unitary tests on this site:
[Postaman - Documentation](https://documenter.getpostman.com/view/8724744/TVCcWUNC/ "Postaman - Documentation")

---
## Requirements

For development, you will only need Node.js and a node global package, installed in your environement.

For production, you will need docker to run commands.

---

## Run Elasticsearch Commands

Pull Elasticseach >= 7.5

    docker pull docker.elastic.co/elasticsearch/elasticsearch:7.5.2

Run this in  localhost port 9200 or configure this in .env file

    docker run -p 9200:9200 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.5.2

---

## Run && Build

### Mode 1: build and run

	$ cd <your path>/mutant_node_v1
	$ git clone https://matmper@bitbucket.org/matmper/mutant_node_v1.git
    $ docker build -t <user>/mutant_node_v1 .
    $ docker run --name mutant-test-nodejs -p 8080:8080 -d <user>/mutant_node_v1

---

### Mode 2: pull and run

    $ docker pull matmper/mutant_node_v1
    $ docker run --name mutant-test-nodejs -p 8080:8080 -d matmper/mutant_node_v1

---

### Mode Dev: Running the project for development

    $ npm run dev

---

### Mode Test: Running Chai HTTP Test

    $ npm test

