const usersFetch 		= require('../helpers/UsersHelper');

class UsersController {

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	// 1- GET USERS WEBSITES
	async websites(req, res) {

		try {

			const users 	= await usersFetch();

			// map users websites
			const response 	= await users.map(user => user.website);
			
			res.status(200).json(response);

		} catch(e) {

			res.status(400).json({ status: 400, message: e.message });

		}

	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	// 2 - GET USERS WEBSITES
	async profiles(req, res) {

		try {

			const users 		= await usersFetch();

			// map users (name, email and company)
			const users_map 	= users.map(user => {
			  	return {
			  		name: 		user.name,
			  		email: 		user.email,
			  		company: 	user.company.name
			  	}
			});

			// order users by name
			const response 		= await users_map.sort((a, b) => a.name > b.name ? 1 : -1);

			res.status(200).json(response);

		} catch(e) {

			res.status(400).json({ status: 400, message: e.message });
			
		}

	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	// 3 - GET USERS WITH SUITE IN ADDRESS
	async address(req, res) {

		try {

			const users 	= await usersFetch();

			// map users (word suuit in address)
			const response 	= users.filter(user => {
				let suite = user.address.suite;
					suite = suite.toLowerCase();
				if( suite.includes('suite')  ) return user;
			});

			res.status(200).json(response);

		} catch(e) {

			res.status(400).json({ status: 400, message: e.message });
			
		}

	}

}

module.exports = new UsersController;