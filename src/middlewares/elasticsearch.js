const { Client, errors } 		= require('@elastic/elasticsearch');

module.exports 			= function create() {

	console.log(errors)

	return new Client({
		node: process.env.ELASTIC_APM_SERVER_URL,
		maxRetries: 1,
		requestTimeout: 1000 * 10, // 10 segundos
	})

}