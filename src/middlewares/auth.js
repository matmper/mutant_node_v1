const basicAuth 		= require('express-basic-auth');

const auth = basicAuth({
    users: { 
    	user:"Mutant123"
    },
    unauthorizedResponse: getUnauthorizedResponse
})

// Unauthorized Function
function getUnauthorizedResponse(req) {

	const res 			= {};
		  res.success	= false;

	if( req.auth )
		res.message 	= `Your auth is invalid (${req.auth.user}:${req.auth.password})`;
	else
		res.message 	= `Auth is required`;

	return res;

}

module.exports 					= auth;