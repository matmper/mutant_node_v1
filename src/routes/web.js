const { Router } 				= require('express');
const routes 					= Router();

const PagesController 			= require('../controllers/PagesController');
const UsersController 			= require('../controllers/UsersController');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PAGES
routes.get('/', async (req, res) => { 
	await PagesController.index(req, res);
});


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// USERS

// USERS - WEBSITES
routes.get('/users/websites', async (req, res) => { 
	await UsersController.websites(req, res);
});

// USERS - PROFILES
routes.get('/users/profiles', async (req, res) => { 
	await UsersController.profiles(req, res);
});

// USERS - PROFILES
routes.get('/users/address', async (req, res) => { 
	await UsersController.address(req, res);
});

module.exports 				= routes;