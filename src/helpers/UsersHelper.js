const fetch 				= require('node-fetch');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// FETCH USERS
module.exports = async function usersFetch() {

	try {

		const response 	= await fetch("https://jsonplaceholder.typicode.com/users", {
			method: 'GET', redirect: 'follow'
		})
		
		return await response.json();

	// return error (catch)
	} catch(e) {

		console.log(`User Fetch:  ${e}`);
		return false;

	}

}