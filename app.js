require('dotenv/config');

const express 				= require('express');
const cors 					= require('cors');
const http 					= require('http');
const path 					= require('path');

// Middlewares
// const auth 				= require('./src/middlewares/auth');

// Log Elasticsearch
const elasticsearch 		= require('./src/middlewares/elasticsearch');
elasticsearch();

// Routes
const  routes 				= require('./src/routes/web');

const app 					= express();
const server 				= http.Server(app);

app.use(cors());
// app.use(auth);
app.use(express.json());
app.use('/', routes);

const PORT = process.env.PORT || 8080;
server.listen(PORT);